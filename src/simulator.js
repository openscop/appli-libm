import simulatorConfig from './simulator-config';
import Utilities from './utilities';
import Graph from './graph';

const Papa = require('papaparse');
import csvHomme from 'raw-loader!./homme.csv';
import csvFemme from 'raw-loader!./femme.csv';

class SIM {

    _csvIndex(columnName) {
        return this.config.csvColumn.code.indexOf(columnName);
    }

    constructor() {

        // attributes

        this.config = simulatorConfig;
        this.perfA = null;
        this.perfB = null;

        // load csv data

        this.config.performance.homme = Papa.parse(csvHomme, {delimiter: ";", worker: false, fastMode: true}).data;
        this.config.performance.femme = Papa.parse(csvFemme, {delimiter: ";", worker: false, fastMode: true}).data;

        // ui bind

        this.uiSpan = {
            VO2max: document.getElementById("init_VO2max"),
            seuil4mM: document.getElementById("init_seuil4mM"),
            coutNetO2: document.getElementById("init_coutNetO2"),
            wingate: document.getElementById("init_wingate"),
            performanceA: document.getElementById("performanceA"),
            performanceB: document.getElementById("performanceB"),
            distance_info: document.getElementById("distance_info"),
        };
        this.uiSelect = {
            distance: document.getElementById("distance"),
            genre: document.getElementById("genre"),
            VO2max: document.getElementById("VO2max"),
            seuil4mM: document.getElementById("seuil4mM"),
            coutNetO2: document.getElementById("coutNetO2"),
            wingate: document.getElementById("wingate"),
        };
        this.uiBt = {
            calculateA: document.getElementById("calculateA"),
            calculateB: document.getElementById("calculateB"),
        };
        this.uiDiv = {
            danger: document.getElementById("danger"),
            collapseOne: document.getElementById("collapseOne"),
            graphFooter: document.querySelectorAll("#graphDeck .card-footer"),
            performance: document.getElementById("performance"),
        };
        this.uiGraph = {
            graph1A: new Graph(document.getElementById("graph1A"), "left"),
            graph1B: new Graph(document.getElementById("graph1B"), "right"),
            graph2A: new Graph(document.getElementById("graph2A"), "left"),
            graph2B: new Graph(document.getElementById("graph2B"), "right"),
            graph3A: new Graph(document.getElementById("graph3A"), "left"),
            graph3B: new Graph(document.getElementById("graph3B"), "right"),
            graph4A: new Graph(document.getElementById("graph4A"), "left"),
            graph4B: new Graph(document.getElementById("graph4B"), "right"),
        };
        this.uiRadio = {
            radioGraph1: document.querySelectorAll('input[name=radioGraph1]'),
            radioGraph2: document.querySelectorAll('input[name=radioGraph2]'),
            radioGraph3: document.querySelectorAll('input[name=radioGraph3]'),
            radioGraph4: document.querySelectorAll('input[name=radioGraph4]'),
        };
        this.uiRadio.radioGraph1Checked = function () { return document.querySelector('input[name=radioGraph1]:checked').value };
        this.uiRadio.radioGraph2Checked = function () { return document.querySelector('input[name=radioGraph2]:checked').value };
        this.uiRadio.radioGraph3Checked = function () { return document.querySelector('input[name=radioGraph3]:checked').value };
        this.uiRadio.radioGraph4Checked = function () { return document.querySelector('input[name=radioGraph4]:checked').value };

        // on event handlers

        this.uiSelect.genre.addEventListener("change", () => {
            this.perfA = null;
            this.perfB = null;
            this.clearAllGraph();
            // create all the options of the select fields from the configuration
            let genre = this.uiSelect.genre.value;
            Object.keys(this.config.profil[genre]).forEach((param) => {   // get the parameters from the configuration
                this.uiSpan[param].innerText = (param != "seuil4mM") ? this.config.profil[genre][param][0] : this.config.profil[genre][param][0] * 100;  // fill init value
                this.uiSelect[param].options.length = 0;  // clear options of select field
                this.config.profil[genre][param].forEach((val) => {  // get the values from the parameter
                    let opt = document.createElement('option');  // create option
                    opt.value = val;
                    opt.innerHTML = (param != "seuil4mM") ? val : val * 100;
                    this.uiSelect[param].appendChild(opt); // create option to the select field
                });
            });
        });

        this.uiSelect.distance.addEventListener("change", () => {
            this.perfA = null;
            this.perfB = null;
            this.uiDiv.collapseOne.classList.add("show");
            this.uiSpan.distance_info.innerText = "";
            this.clearAllGraph();
        });

        this.uiBt.calculateA.addEventListener("click", () => { this.calculate("A"); })
        this.uiBt.calculateB.addEventListener("click", () => { this.calculate("B"); })

        document.getElementById("radio1Graph1").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 1); }
            if (this.perfB) { this.draw("B", 1); }
        });
        document.getElementById("radio2Graph1").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 1); }
            if (this.perfB) { this.draw("B", 1); }
        });
        document.getElementById("radio1Graph2").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 2); }
            if (this.perfB) { this.draw("B", 2); }
        });
        document.getElementById("radio2Graph2").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 2); }
            if (this.perfB) { this.draw("B", 2); }
        });
        document.getElementById("radio3Graph2").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 2); }
            if (this.perfB) { this.draw("B", 2); }
        });
        document.getElementById("radio4Graph2").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 2); }
            if (this.perfB) { this.draw("B", 2); }
        });
        document.getElementById("radio1Graph3").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 3); }
            if (this.perfB) { this.draw("B", 3); }
        });
        document.getElementById("radio1Graph4").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 4); }
            if (this.perfB) { this.draw("B", 4); }
        });
        document.getElementById("radio2Graph4").addEventListener("change", () => {
            if (this.perfA) { this.draw("A", 4); }
            if (this.perfB) { this.draw("B", 4); }
        });

        window.addEventListener('resize', () => { this.resizeAllGraph() }, false);

    };


    checkParameter() {
        let check = true;
        if (isNaN(this.uiSelect.distance.value)) {
            this.uiSelect.distance.classList.add("is-invalid");
            check = false;
        } else {
            this.uiSelect.distance.classList.remove("is-invalid");
        }
        if (this.uiSelect.genre.value != "homme" && this.uiSelect.genre.value != "femme") {
            this.uiSelect.genre.classList.add("is-invalid");
            check = false;
        } else {
            this.uiSelect.genre.classList.remove("is-invalid");
        }
        return (check == true) ? true : false;
    };


    calculate(profil) {

        if (this.checkParameter()) {

            let foundPerf ;

            // show profil and graphics footer
            setTimeout(() => {
                for (var i = 0; i < this.uiDiv.graphFooter.length; i++) {
                    this.uiDiv.graphFooter[i].classList.remove('d-none')
                }
                setTimeout(() => {
                    this.uiBt.calculateB.classList.remove("d-none");
                }, 500);
            }, 500);

            // search data in data source
            switch (profil) {
                case "A":
                    foundPerf = this.config.performance[this.uiSelect.genre.value].find( (perf) => {
                        return (perf[this._csvIndex("distance")] == this.uiSelect.distance.value &&
                            perf[this._csvIndex("VO2max")] == this.uiSelect.VO2max[0].value &&
                            perf[this._csvIndex("seuil4mM")] == this.uiSelect.seuil4mM[0].value &&
                            perf[this._csvIndex("coutNetO2")] == this.uiSelect.coutNetO2[0].value &&
                            perf[this._csvIndex("wingate")] == this.uiSelect.wingate[0].value)
                    });
                    break;
                case "B":
                    foundPerf = this.config.performance[this.uiSelect.genre.value].find( (perf) => {
                        return (perf[this._csvIndex("distance")] == this.uiSelect.distance.value &&
                            perf[this._csvIndex("VO2max")] == this.uiSelect.VO2max.value &&
                            perf[this._csvIndex("seuil4mM")] == this.uiSelect.seuil4mM.value &&
                            perf[this._csvIndex("coutNetO2")] == this.uiSelect.coutNetO2.value &&
                            perf[this._csvIndex("wingate")] == this.uiSelect.wingate.value)
                        });
                    break
                default:
                    foundPerf = null;
            }

            if (foundPerf) {

                this["perf" + profil] = foundPerf;
                this.uiSpan["performance"+profil].innerHTML = Utilities.toHHMMSS(foundPerf[this._csvIndex("tempsCourseSec")]);
                this.uiSpan.distance_info.innerHTML = "sur " + this.uiSelect.distance.selectedOptions[0].innerHTML;
                this.uiDiv.performance.classList.remove("d-none");
                
                this.draw(profil);
                
            } else {

                this["perf" + profil] = null;
                this.uiDiv.danger.innerHTML = "Nous ne parvenons pas à effectuer cette simulation (pas de donnée correspondante).";
                this.uiDiv.danger.classList.remove("d-none");

            }
        }
    };


    draw(profil, graphNumber = 0) {
        if (graphNumber) {
            this["drawGraph"+graphNumber](profil);
        } else {
            this.drawGraph1(profil);
            setTimeout(() => {
                this.drawGraph2(profil);
                setTimeout(() => {
                    this.drawGraph3(profil);
                    setTimeout(() => {
                        this.drawGraph4(profil);
                    }, 300);
                }, 300);
            }, 300);
        }
    };


    drawGraph1(profil) {

        // profil is "A" or "B"
        let perf = this["perf" + profil];
        let i, label, g = this.uiGraph["graph1" + profil];
        g.clear();

        switch (this.uiRadio.radioGraph1Checked()) {
            case 'vitesseMoyenneKMh':

                g.maxValue = this.config.graphMaxValue.vitesseMoyenneKMh;

                i = this._csvIndex("vitesseMoyenneKMh");
                g.addBar( perf[i] );

                i = this._csvIndex("vitesseSeuil4mM");
                label = this.config.csvColumn.labelCourt[i];
                g.addLine( perf[i], label, "bottom");

                i = this._csvIndex("VMA");
                label = this.config.csvColumn.labelCourt[i];
                g.addLine( perf[i], label);

                i = this._csvIndex("vitesseMoyenneKMh");
                g.addValueText( perf[i] );

                break;

            case 'vitesse100pVMA':

                g.maxValue = this.config.graphMaxValue.vitesse100pVMA;

                i = this._csvIndex("vitesse100pVMA");
                g.addBar( perf[i] );
                g.addValueText( perf[i] );

                break;
        }
    }

    drawGraph2(profil) {

        // profil is "A" or "B"
        let perf = this["perf" + profil];
        let i, label, g = this.uiGraph["graph2" + profil];
        g.clear();

        switch (this.uiRadio.radioGraph2Checked()) {
            case 'VO2moyenMlMinKg':

                g.maxValue = this.config.graphMaxValue.VO2moyenMlMinKg;

                i = this._csvIndex("VO2moyenMlMinKg");
                g.addBar( perf[i] );

                i = this._csvIndex("VO2max");
                label = this.config.csvColumn.labelCourt[i];
                g.addLine( perf[i], label);

                i = this._csvIndex("VO2moyenMlMinKg");
                g.addValueText( perf[i] );

                break;

            case 'VO2moyen100pVO2max':

                g.maxValue = this.config.graphMaxValue.VO2moyen100pVO2max;

                i = this._csvIndex("VO2moyen100pVO2max");
                g.addBar( perf[i] );
                g.addValueText( perf[i] );

                break;

            case 'VO2finCourseMlMinKg':

                g.maxValue = this.config.graphMaxValue.VO2finCourseMlMinKg;

                i = this._csvIndex("VO2finCourseMlMinKg");
                g.addBar( perf[i] );

                i = this._csvIndex("VO2max");
                label = this.config.csvColumn.labelCourt[i];
                g.addLine( perf[i], label);

                i = this._csvIndex("VO2finCourseMlMinKg");
                g.addValueText( perf[i] );

                break;

            case 'VO2finCourse100pVO2max':

                g.maxValue = this.config.graphMaxValue.VO2finCourse100pVO2max;

                i = this._csvIndex("VO2finCourse100pVO2max");
                g.addBar( perf[i] );
                g.addValueText( perf[i] );

                break;
        }
    }

    drawGraph3(profil) {

        // profil is "A" or "B"
        let perf = this["perf" + profil];
        let i, label, g = this.uiGraph["graph3" + profil];
        g.clear();

        switch (this.uiRadio.radioGraph3Checked()) {
            case 'lactateFinCourse':

                g.maxValue = this.config.graphMaxValue.lactateFinCourse;

                i = this._csvIndex("lactateFinCourse");
                g.addBar( perf[i] );

                g.addLine( 2, "2");
                g.addLine( 4, "4");

                i = this._csvIndex("lactateFinCourse");
                g.addValueText( perf[i] );

                break;
        }
    }

    drawGraph4(profil) {

        // profil is "A" or "B"
        let perf = this["perf" + profil];
        let i, label, g = this.uiGraph["graph4" + profil];
        g.clear();

        switch (this.uiRadio.radioGraph4Checked()) {
            case 'aerobie':

                g.maxValue = this.config.graphMaxValue.aerobie;

                i = this._csvIndex("aerobie");
                g.addBar( perf[i] );
                g.addValueText( perf[i] );

                break;

            case 'anaerobie':

                g.maxValue = this.config.graphMaxValue.anaerobie;

                i = this._csvIndex("anaerobie");
                g.addBar( perf[i] );
                g.addValueText( perf[i] );

                break;
        }
    }

    clearAllGraph() {
        for (let g in this.uiGraph) {
            this.uiGraph[g].clear();
        }
        this.uiSpan.performanceA.innerText = "";
        this.uiSpan.performanceB.innerText = "";
    }

    resizeAllGraph() {
        for (let g in this.uiGraph) {
            this.uiGraph[g].expandSize();
        }
        if (this.perfA) { this.draw("A"); } ;
        if (this.perfB) { this.draw("B"); } ;
    }

};


export default SIM;


